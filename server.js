//set the envinroment
process.env.NODE_ENV = process.env.NODE_ENV || 'local';

var net = require('net');
var fs = require('fs');

var app = require('./app');
var config = require('./config/' + process.env.NODE_ENV);


//app.set('port', config.port);
var server = require('http').createServer(app);


server.start = function(){
  server.listen(config.port, function(){
    console.log('Server listening on port %d env %s', config.port, process.env.NODE_ENV);
  });
}

server.on('error', function (error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  switch (error.code) {
    case 'EACCES':
      console.error('Port ' + port + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error('Port ' + port + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});

server.start();

module.exports = server;
