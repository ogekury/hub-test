
angular.module('apiService', [])

	.factory('Api', function($http) {
		return {
			getUsers : function () {
				return $http.get('/api/users');
			},
			getUser: function (id) {
				return $http.get('/api/users/' + id);
			},
			getAccounts : function () {
				return $http.get('/api/accounts/');
			},
			getAccount : function (id) {
				return $http.get('/api/accounts/' + id);
			},
			updateAccount : function (id, accountData) {
				return $http.put('/api/accounts/' + id, {account: accountData});
			}
		}

	});
