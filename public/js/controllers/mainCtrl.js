var socialApp = angular.module('socialApp');

socialApp.controller('MainCtrl', function ($scope, $http, Api) {

  $scope.user = null;
  $scope.features = null;

  // get user
  Api
   .getUser('562a1a832288e1bc0f41ba28')
   .success(function (data) {
     if (data.users) {
       $scope.features = data.users.accountId[0].features;
       $scope.premium = data.users.accountId[0].premiumFeatures;
       $scope.user = data.users;
    }

   });

   //update features
   $scope.updateFeatures = function (name, action) {
     var features = $scope.features,
         account = $scope.user.accountId[0];

     if ($scope.user) {
       // change front
       features[name] = action;
       //change back
       Api
       .updateAccount(account._id, {'features': features})
       .success(function (data) {
         if (data.account) {
           $scope.features = data.account.features;
         }
       });

     }
     return false;
   }



});
