process.env.NODE_ENV = process.env.NODE_ENV || 'local';
var express = require('express');
var cors = require('cors');
var path = require('path');
var mailer = require('express-mailer');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var compression = require('compression');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cons = require('consolidate');

var config = require('./config/'+process.env.NODE_ENV );




var app = express();

// Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// --------------------------------------------------------------------------
// Generic middle-wares

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public"), {maxage: '1h'}));


app.use('/', require('./routes/index'));
app.use('/api/accounts', require('./routes/api/accounts'));
app.use('/api/users', require('./routes/api/users'));

module.exports = app;
