var driver = require('mongoose');
var fs = require('fs');
var path = require('path');

var config = require('../config/' + process.env.NODE_ENV);

var options = {
  db: {
    native_parser: true
  }
};


console.log('[MONGODB] Connecting...');

var connection = driver.connect(config.mongo.path, function(err){
  if (err) {
    console.error('[MONGODB] Connection error', err);
  } else {
    console.log('[MONGODB] Connected!');
  }
});

module.exports = connection;
