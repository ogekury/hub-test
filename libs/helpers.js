// helpers file
var mongoose = require('mongoose');
var _ = require('lodash');


var helpers = {
  toObjectId: function (stringId) {
    try {
      return mongoose.Types.ObjectId(stringId);
    } catch (err) {
      console.error(err.message);
      return null;
    }
  },
  updateAccountFields: function (account, reqAccount) {
    _.forEach(reqAccount, function (el, key) {
      account[key] = el;
    });

    return account;
  }
}

module.exports = helpers;
