var async = require('async');
var express = require('express');
var router = express.Router();

// models
var User = require('../../models/users');

var Helpers = require('../../libs/helpers');

router.get('/', function (req, res) {

  User
  .find({})
  .populate('accountId')
  .exec(function (err, users) {
    if (err) {
      return res.status(500).send(err);
    } else if (!users) {
      return res.status(500).send({'error': 'users empty'});
    }

    res.status(200).send({ users: users });
  });

});

// --------------------------------------------------------------------------

router.get('/:user', function (req, res) {

  var userId = (Helpers.toObjectId(req.params.user)) ? req.params.user : null;

  if (!userId) {
    res.status(404).send({'error': 'wrong input'});
  }

  User
  .findById(userId)
  .populate('accountId')
  .exec(function (err, users) {
    if (err) {
      return res.status(500).send(err);
    } else if (!users) {
      return res.status(500).send({'error': 'users empty'});
    }

    res.status(200).send({ users: users });
  });

});

module.exports = router;
