var async = require('async');
var express = require('express');
var router = express.Router();
var _ = require('lodash');


var Account = require('../../models/accounts');

var Helpers = require('../../libs/helpers');

// --------------------------------------------------------------------------

router.get('/', function (req, res) {

  Account.find({}, function (err, accounts) {
    if (err) {
      return res.status(500).send(err);
    } else if (!accounts) {
      return res.status(500).send({'error': 'accounts not found'});
    }

    res.status(200).send({ accounts: accounts });
  });

});

// --------------------------------------------------------------------------

router.get('/:account', function (req, res) {
  var accountId = (Helpers.toObjectId(req.params.account)) ? req.params.account : null;

  if (!accountId) {
    res.status(404).send({'error': 'wrong input'});
  }

  Account.findById(accountId, function (err, accounts) {
    if (err) {
      return res.status(500).send(err);
    } else if (!accounts) {
      return res.status(500).send({'error': 'accounts not found'});
    }

    res.status(200).send({ account: accounts });
  });

});

// --------------------------------------------------------------------------

router.put('/:account', function (req, res) {
  var reqAccount = req.body.account || null,
      accountId = (Helpers.toObjectId(req.params.account)) ? req.params.account : null;

  // check if amends are correct
  if (!reqAccount || !accountId) {
    res.status(404).send({'error': 'wrong input'});
  }

  /*
  using waterfall rather than mongoose findByIdAndUpdate so
  someone can eventually add a pre-save hook to check if the data is correct
  */
  async.waterfall([
    // find user
    function (next) {

      Account.findById(accountId, function (err, account) {
        if (err) {
          res.status(500).send(error);
        }
        next(err, account);
      });
    },
    //update field and save
    function (account) {
      Helpers.updateAccountFields(account, reqAccount);

      account.save(function (err, saved) {
        if (err) {
          return res.status(500).send(error);
        }
        res.status(200).send({ account: saved });
      });
    }
  ]);
});



module.exports = router;
