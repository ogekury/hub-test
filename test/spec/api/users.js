var User = require('../../../models/users');


describe('User', function () {

  describe('Getting the user profile', function () {
    var user;

    beforeEach(function (next) {
      user = new User({
        email: 'test@example.org',
        password: 'my very long cool password',
        username: 'hans'
      });

      user.save(function (err, save) {
        user = save;
        next();
      });
    });

    after(function (done) {
      User.db.db.dropCollection('users', function(err, result) {
        done();
      });
    });

    it('Should return some list of users', function (done) {
      client
        .get('api/users')
        .end(function (err, res) {
          expect(res.status).to.be(200);
          done();
        });
    });

    it('Should return some details about the user', function (done) {
      client
        .get('api/users/' + user._id)
        .end(function (err, res) {
          expect(res.status).to.be(200);
          expect(res.body.users.username).to.be('hans');
          done();
        });
    });

  });
});
