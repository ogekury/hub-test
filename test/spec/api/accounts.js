var Account = require('../../../models/accounts');


describe('Accounts', function () {

  describe('Getting the account', function () {
    var account;

    beforeEach(function (next) {
      account = new Account({
        hubVersion: '1.1',
        name: 'account1',
        locale: 'de'
      });

      account.save(function (err, saved) {
        account = saved;
        next();
      });
    });

    after(function (done) {
      Account.db.db.dropCollection('acounts', function(err, result) {
        done();
      });
    });

    it('Should return a list of accounts', function (done) {
      client
        .get('api/accounts')
        .end(function (err, res) {
          expect(res.status).to.be(200);
          done();
        });
    });

    it('Should return some details about the account', function (done) {
      client
        .get('api/accounts/' + account._id)
        .end(function (err, res) {
          expect(res.status).to.be(200);
          expect(res.body.account.name).to.be('account1');
          done();
        });
    });

    it('Should put new details about the account', function (done) {
      client
        .put('api/accounts/' + account._id)
        .send({ 'account': { 'name': 'account2' }})
        .end(function (err, res) {
          expect(res.status).to.be(200);
          expect(res.body.account.name).to.be('account2');
          done();
        });
    });

  });
});
