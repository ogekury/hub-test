var superagent = require("superagent");
var config = require('../../config/' + process.env.NODE_ENV);

module.exports = {
  del: function (path, fn) {
    return (superagent.del(config.host + path, fn));
  },

  get: function (path, data, fn) {
    return (superagent.get(config.host + path, data, fn));
  },

  patch: function (path, data, fn) {
    return (superagent.patch(config.host + path, data, fn));
  },

  post: function (path, data, fn) {
    return (superagent.post(config.host + path, data, fn));
  },

  put: function (path, data, fn) {
    return (superagent.put(config.host + path, data, fn));
  }
}
