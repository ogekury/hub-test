// Env vars
process.env.NODE_ENV = 'tests';

// Global libs
global.async = require('async');
global.casual = require('casual');
global.expect = require('expect.js');
global.httpMocks = require('node-mocks-http');

// Global libs from our framework
global.client = require('./libs/client');

// API specs
require('./spec/api/users');
require('./spec/api/accounts');

// Starts the server
var server = require('../server');
server.start();
