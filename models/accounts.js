var async = require('async');
var _ = require('lodash');

var db = require('../libs/db');

var accountSchema = new db.Schema({
  createdTime: {
    type: Date,
  },
  hubVersion: {
    type: String
  },
  package: {
    type: String
  },
  expirationTime: {
    type: Date,
  },
  tags: {
    type: [String]
  },
  history: [
    {
      type: {
        type: String
      },
      time: {
        type: Date
      }
    }
  ],
  features: {
    approvalProcess: {
      type: Boolean
    },
    insightsUserActions: {
      type: Boolean
    },
    coworking: {
      type: Boolean
    },
    categories: {
      type: Boolean
    },
    categoriesForceUserToTag: {
      type: Boolean
    },
    templates: {
      type: Boolean
    },
    cp_beta: {
      type: Boolean
    }
  },
  premiumFeatures : {
    security: {
      type: Boolean
    }
  },
  name: String,
  locale: String,
  appId: String,
  realtimeAppId: String
}, {
  toObject: {
    transform: function (document, returns) {
      // Prevent the __v property getting returned
      delete returns.__v;
    }
  }
});

module.exports = db.model('Account', accountSchema);
