var async = require('async');
var _ = require('lodash');

var db = require('../libs/db');

var userSchema = new db.Schema({
  settings: {
    general : {
      showAllTicketTab: {
        type : Boolean
      }
    },
    sidebarRight : {
      showUserFeedFirst : {
        type : Boolean
      },
      commentBarrier : {
        type: Number
      },
    },
    emails : {
      newTicket : {
        type: Boolean
      },
      sendAssignNotification : {
        type: String
      }
    }
  },
  locale: {
    type: String
  },
  history: {
    type: [String]
  },
  lastLoginTime: {
    type: Date
  },
  accountId: [{
      type: db.Schema.Types.ObjectId,
      ref: 'Account'
    }
  ],
  userVerifyOneTimeToken: {
    type: String
  },
  expirationTime: {
    type: Date
  },
  accessToken: {
    type: String
  },
  lastPasswordChange: {
    type: Date
  },
  password: {
    type: String
  },
  lastName: {
    type: String
  },
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String
  },
  username: {
    type: String
  },
  updateTime: {
    type: Date
  },
  failedLogins: {
    type: [String]
  },
  usedOneTimeTokens: {
    type: [String]
  },
  createdTime: {
    type: Date
  }

}, {
  toObject: {
    transform: function (document, returns) {
      // Prevent the __v property getting returned
      delete returns.__v;
    }
  }
});

module.exports = db.model('User', userSchema);
